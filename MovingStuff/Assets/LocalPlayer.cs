﻿using UnityEngine;
using System.Collections;

public class LocalPlayer : MonoBehaviour
{

    public static LocalPlayer Instance;

    public Vector2 ActiveBlock;

    // Use this for initialization
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        ActiveBlock = new Vector2( ( ( ( (int)transform.position.x + (int)( Mathf.Sign( transform.position.x ) * ( MovingMesh.Instance.planeSizeX / 2 ) ) ) / MovingMesh.Instance.planeSizeX ) ), ( (int)transform.position.z + (int)( Mathf.Sign( transform.position.z ) * ( MovingMesh.Instance.planeSizeY / 2 ) ) ) / MovingMesh.Instance.planeSizeY );
        //ActiveBlock += new Vector2(  / 2,MovingMesh.Instance.planeSizeY / 2 );
    }
}

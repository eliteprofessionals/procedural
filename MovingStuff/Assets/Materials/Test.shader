﻿Shader "Custom/Test" {
	Properties
	{
		_MainTex( "Snow", 2D ) = "white" {}
		_MainTex2( "Grass", 2D ) = "white" {}
		_CliffTexture( "Rock", 2D ) = "white" {}
		_HeightMin( "Height Min", Float ) = -1
		_HeightMax( "Height Max", Float ) = 1
		_GrassCutoff( "Grass Cutoff", Float ) = .5
		_CliffSteepness( "Cliff Steepness", Float ) = .5
		_TextureScale( "Texture Scale",Float ) = 1
		_TriplanarBlendSharpness( "Blend Sharpness",Float ) = 1
		_Amount( "Extrusion Amount", Range( 0,5 ) ) = 0.5
		_SnowCrumpled( "SnowCrupled", 2D ) = "black" {}

		_DispTex( "Disp Texture", 2D ) = "gray" {}
		_NormalMap( "Normalmap", 2D ) = "bump" {}
		_Displacement( "Displacement", Range( 0, 1.0 ) ) = 0.3
	}

		SubShader
		{
			Tags{ "RenderType" = "Opaque" }

			CGPROGRAM
	#pragma surface surf Lambert vertex:vert

			sampler2D _MainTex;
			sampler2D _MainTex2;
			sampler2D _CliffTexture;
			sampler2D _SnowCrumpled;
			sampler2D _DispTex;

			float _Displacement;
			float _Amount;

			float _HeightMin;
			float _HeightMax;

			float _CliffSteepness;
			float _GrassCutoff;
			fixed4 _ColorMin;
			fixed4 _ColorMax;

			float _TextureScale;
			float _TriplanarBlendSharpness;

			struct Input
			{
				float2 uv_MainTex;
				float3 worldPos;
				fixed3 worldNormal;
			};



			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			v2f vert( inout appdata_base v )
			{
				v2f o;
				o.vertex = UnityObjectToClipPos( v.vertex );

				float4 tex = tex2Dlod( _SnowCrumpled, float4( v.texcoord.xy, 0, 0 ) );
				float d = tex2Dlod( _DispTex, float4( v.texcoord.xy, 0, 0 ) ).r * _Displacement;

				float amount = ( v.normal.y * v.normal.y ) * _Amount;
				float h = ( _HeightMax - v.vertex.y ) / ( _HeightMax - _HeightMin );
				//tesselate
				v.vertex.xyz += v.normal * d;
				if( h < 1 - _GrassCutoff )
				{
				if( ( 1 - v.normal.y ) < _CliffSteepness - .07 ) //.07 to get away from the edge and not affect non snowy values.
				{
				   v.vertex.y += amount;
				}
				if( tex.a > 0 )
				{
					v.vertex.y -= amount * tex.a;
					v.vertex.xyz -= v.normal * d;
				}
				}

				return o;
			}

			void surf( Input IN, inout SurfaceOutput o )
			{
				// Find our UVs for each axis based on world position of the fragment.
				half2 yUV = IN.worldPos.xz / _TextureScale;
				half2 xUV = IN.worldPos.zy / _TextureScale;
				half2 zUV = IN.worldPos.xy / _TextureScale;

				half4 c3 = tex2D( _SnowCrumpled, IN.uv_MainTex );
				float h = ( _HeightMax - IN.worldPos.y ) / ( _HeightMax - _HeightMin );

				if( h < 1 - _GrassCutoff )
				{
					
					h *= .5;
				}
				else
				{
					c3.a = 0;
					h *= 2;
				}

				h = clamp( h, 0, 1 );

				float steepness = 1 - IN.worldNormal.y;

				half3 yDiff = tex2D( _MainTex, yUV );
				half3 xDiff = tex2D( _MainTex, xUV );
				half3 zDiff = tex2D( _MainTex, zUV );

				yDiff *= 1 - h;
				xDiff *= 1 - h;
				zDiff *= 1 - h;
				yDiff += tex2D( _MainTex2, yUV ) * ( h );
				xDiff += tex2D( _MainTex2, xUV ) * ( h );
				zDiff += tex2D( _MainTex2, zUV ) * ( h );

				if( c3.a > 0 )
				{
					steepness = c3.a * .7;
				}

				if( steepness >= _CliffSteepness )
				{
					steepness *= 2;

					steepness = clamp( steepness, 0, 1 );

					yDiff *= 1 - steepness;
					xDiff *= 1 - steepness;
					zDiff *= 1 - steepness;

					yDiff += tex2D( _CliffTexture, yUV ) * steepness;
					xDiff += tex2D( _CliffTexture, xUV ) * steepness;
					zDiff += tex2D( _CliffTexture, zUV ) * steepness;
				}



				half3 blendWeights = pow( abs( IN.worldNormal ), _TriplanarBlendSharpness );
				// Divide our blend mask by the sum of it's components, this will make x+y+z=1
				blendWeights = blendWeights / ( blendWeights.x + blendWeights.y + blendWeights.z );
				// Finally, blend together all three samples based on the blend mask.
				o.Albedo = xDiff * blendWeights.x + yDiff * blendWeights.y + zDiff * blendWeights.z;

				o.Alpha = IN.worldPos.y;
			}
				ENDCG
		}
			FallBack "Diffuse"
}



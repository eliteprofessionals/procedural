﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class MakeNoise : MonoBehaviour
{
    public Material AddValueToMaterial;

    public float[] Intensity = new float[10];
    public Vector4[] VertexPos = new Vector4[10];
    public int FixedLength = 10;
    public float timer = 1;
    public float timeMultiplier;
    private int Count = 0;

    // Use this for initialization
    void Start()
    {
        Intensity = new float[FixedLength];
        VertexPos = new Vector4[FixedLength];
    }

    void FixedUpdate()
    {
        Material mat = AddValueToMaterial;
        if( mat != null )
        {
            for( int i = 0; i < Intensity.Length; i++ )
            {
                Intensity[i] -= Time.fixedDeltaTime * timeMultiplier;

                if( Intensity[i] < 0 )
                {
                    Intensity[i] = 0;
                }
                else
                {
                    AddValueToMaterial.SetFloatArray( "_Properties", Intensity );
                }
            }
        }
    }

    void Update()
    {
        if( Input.GetKeyDown( KeyCode.Space ) )
        {
            Vector3 Pos;
            DoHit( out Pos );

            if( Pos != null )
            {
                for( int i = 0; i < FixedLength; i++ )
                {
                    if( Intensity[i] == 0 )
                    {
                        VertexPos[i] = Pos;
                        Intensity[i] = timer;
                        AddValueToMaterial.SetInt( "_Points_Length", FixedLength );
                        AddValueToMaterial.SetFloatArray( "_Properties", Intensity );
                        AddValueToMaterial.SetVectorArray( "_Points", VertexPos );
                        return;
                    }
                }
            }
        }
    }

    public void DoHit( out Vector3 vertexPos )
    {
        vertexPos = Vector3.zero;
        RaycastHit hit;
        if( !Physics.Raycast( Camera.main.ScreenPointToRay( Input.mousePosition ), out hit ) )
            return;

        MeshCollider meshCollider = hit.collider as MeshCollider;
        if( meshCollider == null || meshCollider.sharedMesh == null )
            return;

        Mesh mesh = meshCollider.sharedMesh;
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;
        Vector3 p0 = vertices[triangles[hit.triangleIndex * 3 + 0]];
        Vector3 p1 = vertices[triangles[hit.triangleIndex * 3 + 1]];
        Vector3 p2 = vertices[triangles[hit.triangleIndex * 3 + 2]];
        vertexPos = p0;
        Transform hitTransform = hit.collider.transform;
        p0 = hitTransform.TransformPoint( p0 );
        p1 = hitTransform.TransformPoint( p1 );
        p2 = hitTransform.TransformPoint( p2 );

        Debug.DrawLine( p0, p1 );
        Debug.DrawLine( p1, p2 );
        Debug.DrawLine( p2, p0 );
    }


}

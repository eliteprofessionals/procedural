﻿Shader "Dissolving" {
	Properties{
		_MainTex( "Texture (RGB) Alpha (A)", 2D ) = "blue" {}
		_SliceGuide( "Slice Guide (RGB)", 2D ) = "white" {}
		_SliceAmount( "Slice Amount", Range( 0.0, 1.0 ) ) = 0.5
		_BurnSize( "Burn Size", Range( 0.0, 1.0 ) ) = 0.15
		_BurnRamp( "Burn Ramp (RGB)", 2D ) = "white" {}
	}
		SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Cull Off
		CGPROGRAM
		//if you're not planning on using shadows, remove "addshadow" for better performance
#pragma surface surf Lambert addshadow alpha vertex:vert
	struct Input
	{
		float2 uv_MainTex;
		float2 uv_SliceGuide;
		float _SliceAmount;
		float3 localPos;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	uniform int _Points_Length = 0;
	uniform float3 _Points[10];		// (x, y, z) = position //If a script overwrites the length once it will stay that length.
	uniform float _Properties[10];	// x = intensity

	void vert( inout appdata_base v, out Input o )
	{
		UNITY_INITIALIZE_OUTPUT( Input, o );
		//for (int i = 0 ; i < _Points_Length;i++)
		//{
		//	if (length(v.vertex.xyz - _Points[i]) < 1)
		//	{
		//		v.vertex.y = v.normal.y * _Properties[i];
		//	}
		//}
		o.localPos = v.vertex.xyz;
	}

	sampler2D _MainTex;
	sampler2D _SliceGuide;
	float _SliceAmount;
	sampler2D _BurnRamp;
	float _BurnSize;


	void surf( Input IN, inout SurfaceOutput o )
	{
		float4 TextureValues = tex2D( _MainTex, IN.uv_MainTex ).rgba;

		o.Albedo = TextureValues;
		o.Emission = 0;
		o.Alpha = TextureValues.a;

		int latest = -1;
		float latestProp = 0;
		for( int i = 0; i < _Points_Length; i++ )
		{
			if( _Properties[i] <= 0.01 )
			{
				continue;
			}

			float magnitude = length( IN.localPos.xyz - _Points[i] );
			float expandCircle = cos( magnitude *  _Properties[i] ) * 5;

			if( magnitude < 2 + expandCircle )
			{
				float mixer = clamp( _Properties[i], 0, 1 );
				float4 MixValues = tex2D( _BurnRamp, IN.uv_MainTex ).rgba * mixer;

				o.Albedo = MixValues;
				o.Albedo = ( 1 - mixer ) * TextureValues;

				half test = tex2D( _SliceGuide, IN.uv_MainTex ).rgba * ( 1 - mixer );
				if( test < _BurnSize && mixer > 0 && mixer < 1 )
				{
					o.Emission = 1 - tex2D( _BurnRamp, float2( test *( 1 / _BurnSize ), 0 ) );
					o.Albedo *= o.Emission;
				}
			}
		}
	}

	ENDCG
	}
		Fallback "Diffuse"
}

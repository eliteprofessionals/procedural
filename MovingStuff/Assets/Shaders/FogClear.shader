﻿Shader "Custom/ClearFog"
{
    SubShader 
    {
        Pass
        {
            ZTest Always Cull Off ZWrite Off
            ColorMask R
            SetTexture [_Dummy] 
            { 
                constantColor(0, 0, 0, 0) combine constant 
            }
        }
    }
} 
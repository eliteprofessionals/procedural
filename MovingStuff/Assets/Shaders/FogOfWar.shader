﻿Shader "Unlit/FogOfWar"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("ShadowColor", Color) = ( 0.2, 0.2, 0.2, 1 )
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent+1"}
		Blend SrcAlpha OneMinusSrcAlpha
		
		ZWrite Off
		ZTest Always

		Pass {
			ColorMask 0
			Stencil
			{
				Ref 1
				Pass Replace
			}
		}

		Pass
		{
			Stencil
			{
				Ref 1
				Comp Equal
				Pass DecrSat
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			// #pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color; 
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			half4 frag (v2f i) : SV_Target
			{
				half4 fog = tex2D(_MainTex, i.uv);
				

				return half4( 0, 0, 0, 1.0 - max( fog.r, fog.g / 2.0 ) );
			}
			ENDCG
		}
	}
}

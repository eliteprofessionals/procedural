﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;


public class SimpleMouseLook : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float x = CrossPlatformInputManager.GetAxisRaw( "Mouse X" );
        float y = -CrossPlatformInputManager.GetAxisRaw( "Mouse Y" );

        transform.eulerAngles -= new Vector3(0,0,transform.eulerAngles.z);
        transform.rotation *= Quaternion.Euler( new Vector3( y, x, 0 ) );
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingMesh : MonoBehaviour
{
    public static MovingMesh Instance;

    public int planeSizeX;
    public int planeSizeY;

    public int WorldSizeX;
    public int WorldSizeY;

    public int HighLodVertexMultiplier;
    public int LowLodVertexMultiplier;

    public float PerlinScale = 5f;

    public RenderTexture SnowTexture;

    public Material groundMaterial;
    public Material currentInstance;

    public Transform followTransform;

    public float DiscoverRadius = 64f;

    public LinearMap2D<GameObject> HighLodObjects;
    public LinearMap2D<GameObject> LowLodObjects;

    public bool HighLodHasCollider = true;

    [SerializeField]
    private Int2 worldOffset = Int2.zero;

    public Material NewMaterialInstance()
    {
        currentInstance = new Material( groundMaterial );
        groundMaterial.CopyPropertiesFromMaterial( currentInstance );
        currentInstance.SetTexture( "_SnowCrumpled", SnowTexture );
        currentInstance.name = "instanss";
        return currentInstance;
    }

    // Use this for initialization
    void Start()
    {
        Instance = this;

        HighLodObjects = new LinearMap2D<GameObject>( new Int2( WorldSizeX, WorldSizeY ) );
        LowLodObjects = new LinearMap2D<GameObject>( new Int2( 4, 4 ) );

        if( !followTransform.GetComponent<TransformListener>() )
            followTransform.gameObject.AddComponent<TransformListener>();

        followTransform.GetComponent<TransformListener>().hasMoved += ( pos ) =>
        {
            Int2 int2pos = new Int2( (int)followTransform.position.x, (int)followTransform.position.z );
            UpdateHighLod( int2pos );
            UpdateLowLod( int2pos );
        };

        StartWorld( new Int2( (int)followTransform.position.x, (int)followTransform.position.z ) );

    }

    public void StartWorld( Int2 Center )
    {
        Center = new Int2( (int)( (float)Center.x / (float)planeSizeX ), (int)( (float)Center.y / (float)planeSizeY ) );
        for( int x = 0; x < WorldSizeX; x++ )
        {
            for( int y = 0; y < WorldSizeY; y++ )
            {
                HighLodObjects[x, y] = getBlock( new Vector2( x * planeSizeX + Center.x * planeSizeX - ( WorldSizeX / 2 ) * planeSizeX, y * planeSizeY + Center.y * planeSizeY - ( WorldSizeY / 2 ) * planeSizeY ), 1, HighLodVertexMultiplier );

                HighLodObjects[x, y].AddComponent<MeshCollider>();
                HighLodObjects[x, y].GetComponent<MeshCollider>().sharedMesh = HighLodObjects[x, y].GetComponent<MeshFilter>().sharedMesh;
            }
        }

        worldOffset = Center;
    }

    public void UpdateLowLod( Int2 Position )
    {
        Int2 actual = new Int2( (int)( (float)Position.x / (float)planeSizeX ), (int)( (float)Position.y / (float)planeSizeY ) );

        for( int x = 0; x < LowLodObjects.size.x; x++ )
        {
            for( int y = 0; y < LowLodObjects.size.y; y++ )
            {
                if( LowLodObjects[x, y] != null )
                {
                    Destroy( LowLodObjects[x, y] );
                }
            }
        }

        for( int i = 0; i < Int2.Neighbors8.Length; i++ )
        {
            Int2 Index = Int2.zero;
            if( Int2.Neighbors8[i].x != 0 )
            {
                Index.x = 1;
                if( Int2.Neighbors8[i].x > 0 )
                {
                    Index.x = Int2.Neighbors8[i].x * 3;
                }
            }
            if( Int2.Neighbors8[i].y != 0 )
            {
                Index.y = 1;
                if( Int2.Neighbors8[i].y > 0 )
                {
                    Index.y = Int2.Neighbors8[i].y * 3;
                }
            }
            Vector2 getAtPos = new Vector2( (float)Int2.Neighbors8[i].x * planeSizeX * WorldSizeX, (float)Int2.Neighbors8[i].y * planeSizeY * WorldSizeY );
            getAtPos += new Vector2( (float)worldOffset.x * planeSizeX, (float)worldOffset.y * planeSizeY );
            getAtPos -= new Vector2( (float)planeSizeX / 2, (float)planeSizeY / 2 );
            GameObject obj = getBlock( new Vector2( getAtPos.x, getAtPos.y ), WorldSizeX, LowLodVertexMultiplier );
            LowLodObjects[Index] = obj;

            obj.name = "LowLod";
        }
    }

    public void UpdateHighLod( Int2 Position )
    {
        Int2 actual = new Int2( (int)( (float)Position.x / (float)planeSizeX ), (int)( (float)Position.y / (float)planeSizeY ) );
        Int2 Change = actual - worldOffset;

        LinearMap2D<GameObject> newMap = new LinearMap2D<GameObject>( new Int2( WorldSizeX, WorldSizeY ) );
        List<Int2> GetNew = new List<Int2>();
        List<Int2> RemoveAt = new List<Int2>();

        if( Change != Int2.zero )
        {
            worldOffset = actual;

            for( int x = 0; x < WorldSizeX; x++ )
            {
                for( int y = 0; y < WorldSizeY; y++ )
                {
                    if( HighLodObjects.IsInside( x + Change.x, y + Change.y ) )
                    {
                        newMap[x, y] = HighLodObjects[x + Change.x, y + Change.y];
                    }
                    else
                    {
                        GetNew.Add( new Int2( x, y ) );
                    }
                }
            }

            if( Change.x != 0 )
            {
                if( Change.x > 0 )
                {
                    for( int k = Change.x - 1; k >= 0; )
                    {
                        for( int y = 0; y < WorldSizeY; y++ )
                        {
                            RemoveAt.Add( new Int2( k, y ) );
                        }
                        k--;
                    }
                }
                else
                {
                    for( int k = WorldSizeX - 1 + Change.x + 1; k <= WorldSizeX - 1; )
                    {
                        for( int y = 0; y < WorldSizeY; y++ )
                        {
                            RemoveAt.Add( new Int2( k, y ) );
                        }
                        k++;
                    }
                }
            }
            if( Change.y != 0 )
            {
                if( Change.y > 0 )
                {
                    for( int k = Change.y - 1; k >= 0; )
                    {
                        for( int x = 0; x < WorldSizeX; x++ )
                        {
                            RemoveAt.Add( new Int2( x, k ) );
                        }
                        k--;
                    }
                }
                else
                {
                    for( int k = WorldSizeY - 1 + Change.y + 1; k <= WorldSizeY - 1; )
                    {
                        for( int x = 0; x < WorldSizeX; x++ )
                        {
                            RemoveAt.Add( new Int2( x, k ) );
                        }
                        k++;
                    }
                }
            }


            foreach( Int2 value in RemoveAt )
            {
                Destroy( HighLodObjects[value.x, value.y] );
            }

            foreach( Int2 value in GetNew )
            {
                GameObject highlodObj = getBlock( new Vector2( value.x * planeSizeX + actual.x * planeSizeX - ( WorldSizeX / 2 ) * planeSizeX, value.y * planeSizeY + actual.y * planeSizeY - ( WorldSizeY / 2 ) * planeSizeY ), 1, HighLodVertexMultiplier );
                newMap[value.x, value.y] = highlodObj;
                highlodObj.AddComponent<MeshCollider>();
                highlodObj.GetComponent<MeshCollider>().sharedMesh = highlodObj.GetComponent<MeshFilter>().sharedMesh;
                highlodObj.name = "High Lod Object";
            }

            HighLodObjects = newMap;
        }
    }

    public GameObject getBlock( Vector2 offset, float Scale = 1f, int vertexAccuracy = 5 )
    {
        GameObject p = new GameObject();
        p.transform.position = new Vector3( offset.x, 0, offset.y );

        Vector2 pos = new Vector2( p.transform.position.x, p.transform.position.z );
        p.AddComponent<MeshFilter>();
        p.AddComponent<MeshRenderer>();
        p.AddComponent<WorldBlock>();

        p.GetComponent<MeshFilter>().sharedMesh = CreateMesh( vertexAccuracy - 1, vertexAccuracy - 1, planeSizeX * Scale, planeSizeY * Scale, pos );
        p.GetComponent<MeshRenderer>().material = groundMaterial;

        p.GetComponent<WorldBlock>().xPos = (int)offset.x;
        p.GetComponent<WorldBlock>().yPos = (int)offset.y;
        return p;
    }

    Mesh CreateMesh( int widthSegments, int lengthSegments, float width, float length, Vector2 worldOffset )
    {
        Vector2 anchorOffset = Vector2.zero;
        Mesh m = new Mesh();

        int hCount2 = widthSegments + 1;
        int vCount2 = lengthSegments + 1;
        int numTriangles = widthSegments * lengthSegments * 6;

        int numVertices = hCount2 * vCount2;

        Vector3[] vertices = new Vector3[numVertices];
        Vector2[] uvs = new Vector2[numVertices];
        int[] triangles = new int[numTriangles];
        Vector4[] tangents = new Vector4[numVertices];
        Vector4 tangent = new Vector4( 1f, 0f, 0f, -1f );

        int index = 0;
        float uvFactorX = 1.0f / widthSegments;
        float uvFactorY = 1.0f / lengthSegments;
        float scaleX = width / widthSegments;
        float scaleY = length / lengthSegments;
        for( float y = 0.0f; y < vCount2; y++ )
        {
            for( float x = 0.0f; x < hCount2; x++ )
            {
                float xValue = x * scaleX - width / 2f - anchorOffset.x;
                float yValue = y * scaleY - length / 2f - anchorOffset.y;
                vertices[index] = new Vector3( xValue, getHeight( ( worldOffset + new Vector2( xValue, yValue ) ) ) * PerlinScale, yValue );
                tangents[index] = tangent;
                uvs[index++] = new Vector2( x * uvFactorX, y * uvFactorY );
            }
        }

        index = 0;

        for( int y = 0; y < lengthSegments; y++ )
        {
            for( int x = 0; x < widthSegments; x++ )
            {
                triangles[index] = ( y * hCount2 ) + x;
                triangles[index + 1] = ( ( y + 1 ) * hCount2 ) + x;
                triangles[index + 2] = ( y * hCount2 ) + x + 1;

                triangles[index + 3] = ( ( y + 1 ) * hCount2 ) + x;
                triangles[index + 4] = ( ( y + 1 ) * hCount2 ) + x + 1;
                triangles[index + 5] = ( y * hCount2 ) + x + 1;
                index += 6;
            }
        }

        m.vertices = vertices;
        m.uv = uvs;
        m.triangles = triangles;
        m.tangents = tangents;
        m.RecalculateNormals();

        return m;
    }

    public float getHeight( Vector2 worldPosition )
    {
        worldPosition *= .01f;
        return Mathf.PerlinNoise( worldPosition.x, worldPosition.y );
    }
}
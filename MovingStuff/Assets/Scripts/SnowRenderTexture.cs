﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class SnowRenderTexture : MonoBehaviour
{
    public RenderTexture FrontBuffer;
    public Material ClearMaterial;

    public LinearMap2D<byte> FogOfWarValues;
    public Transform MoveTransform;
    public Transform SnowPlane;

    protected new Camera camera;

    private Vector2 old;

    void Start()
    {
        camera = GetComponent<Camera>();

        camera.targetTexture = FrontBuffer;
        FrontBuffer.width = MovingMesh.Instance.HighLodVertexMultiplier * MovingMesh.Instance.HighLodVertexMultiplier;
        FrontBuffer.height = MovingMesh.Instance.HighLodVertexMultiplier * MovingMesh.Instance.HighLodVertexMultiplier;

        Vector2 Center = LocalPlayer.Instance.ActiveBlock;
        camera.transform.position = new Vector3( Center.x, 50, Center.y );

        Initialize( FogOfWarValues );
    }

    public void Initialize( LinearMap2D<byte> values )
    {
        if( values == null ) return;

        Texture2D source = new Texture2D( FrontBuffer.width, FrontBuffer.height, TextureFormat.ARGB32, false, false );

        Color32[] colors = source.GetPixels32();

        RectangleFillIterator iterator = new RectangleFillIterator( Int2.zero, new Int2( FrontBuffer.width, FrontBuffer.height ) );
        foreach( Int2 position in iterator )
        {
            colors[position.y + position.x * FrontBuffer.height].g = values[position];
        }

        source.SetPixels32( colors );
        source.Apply( false, true );

        Graphics.Blit( source, FrontBuffer );
    }

    public LinearMap2D<byte> GetValues()
    {
        LinearMap2D<byte> result = new LinearMap2D<byte>( new Int2( FrontBuffer.width, FrontBuffer.height ) );

        Texture2D buffer = new Texture2D( FrontBuffer.width, FrontBuffer.height, TextureFormat.ARGB32, false, false );
        RenderTexture.active = FrontBuffer;
        buffer.ReadPixels( new Rect( 0, 0, FrontBuffer.width, FrontBuffer.height ), 0, 0 );
        RenderTexture.active = null;

        Color32[] colors = buffer.GetPixels32();
        RectangleFillIterator iterator = new RectangleFillIterator( Int2.zero, new Int2( FrontBuffer.width, FrontBuffer.height ) );
        foreach( Int2 position in iterator )
        {
            result[position] = colors[position.y + position.x * FrontBuffer.height].g;
        }

        return result;
    }

    public void FixedUpdate()
    {
        RenderTexture.active = FrontBuffer;

        Vector2 Center = LocalPlayer.Instance.ActiveBlock;
        MoveTransform.transform.position = new Vector3( Center.x * MovingMesh.Instance.planeSizeX, 100, Center.y * MovingMesh.Instance.planeSizeY );
        if( old != Center )
        {
            foreach( GameObject b in MovingMesh.Instance.HighLodObjects.data )
            {
                WorldBlock wb = b.GetComponent<WorldBlock>();
                if( wb.xPos == (int)Center.x * MovingMesh.Instance.planeSizeX && wb.yPos == (int)Center.y * MovingMesh.Instance.planeSizeY )
                {
                    b.GetComponent<MeshRenderer>().material = MovingMesh.Instance.NewMaterialInstance();
                }
            }

            old = Center;

            SnowPlane.transform.localPosition = new Vector3( 0, 0, 0 );
            SnowPlane.transform.localPosition -= new Vector3( SnowPlane.GetComponent<SnowFog>().planeSize / 2, 0, SnowPlane.GetComponent<SnowFog>().planeSize / 2 );
            GL.Clear( true, true, new Color( 0, 0, 0, 0 ) );
        }


        GL.PushMatrix();
        GL.LoadOrtho();
        ClearMaterial.SetPass( 0 );
        GL.Begin( GL.QUADS );
        GL.Vertex3( 0f, 0f, 0.1f );
        GL.Vertex3( 1f, 0f, 0.1f );
        GL.Vertex3( 1f, 1f, 0.1f );
        GL.Vertex3( 0f, 1f, 0.1f );
        GL.End();
        GL.PopMatrix();
        RenderTexture.active = null;
    }
}

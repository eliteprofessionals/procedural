﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent( typeof( Camera ))]
public class CameraReplacementShader : MonoBehaviour
{
    public Shader Replacement;

    void Start()
    {
        GetComponent<Camera>().SetReplacementShader( Replacement, "" );
    }
}

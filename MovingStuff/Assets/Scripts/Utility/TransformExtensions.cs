﻿using UnityEngine;
using System.Collections;

public static class TransformExtensions
{
	public static void SetLayer( this Transform transform, int layer )
	{
		transform.gameObject.layer = layer;
		foreach( Transform child in transform )
		{
			child.SetLayer( layer );
		}
	}

    //public static Transform GetTopLevel( this Transform trans )
    //{
    //    Transform value = trans;

    //    while( value.parent != null )
    //    {
    //        value = value.parent;
    //    }

    //    return value;
    //}
}
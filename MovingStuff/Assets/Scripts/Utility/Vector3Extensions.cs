﻿using UnityEngine;
using System.Collections;

public static class Vector3Extensions
{
    public static void MakeMinMax( ref Vector3 min, ref Vector3 max )
    {
        if( min.x > max.x )
            MathUtil.Swap( ref min.x, ref max.x );

        if( min.y > max.y )
            MathUtil.Swap( ref min.y, ref max.y );

        if( min.z > max.z )
            MathUtil.Swap( ref min.z, ref max.z );
    }

    public static Vector3 Div( this Vector2 A, Vector2 B )
    {
        return new Vector3( A.x / B.x, A.y / B.y );
    }

    public static Vector3 Div( this Vector3 A, Vector3 B )
    {
        return new Vector3( A.x / B.x, A.y / B.y, A.z / B.z );
    }

    public static Vector3 Mul( this Vector3 A, Vector3 B )
    {
        return new Vector3( A.x * B.x, A.y * B.y, A.z * B.z );
    }

    public static Vector3 Round( this Vector3 r )
    {
        return new Vector3( Mathf.Round( r.x ), Mathf.Round( r.y ), Mathf.Round( r.z ) );
    }

    public static Vector3 Ceil( this Vector3 v )
    {
        return new Vector3( Mathf.Ceil( v.x ), Mathf.Ceil( v.y ), Mathf.Ceil( v.z ) );
    }

    public static Vector3 Floor( this Vector3 v )
    {
        return new Vector3( Mathf.Floor( v.x ), Mathf.Floor( v.y ), Mathf.Floor( v.z ) );
    }

    public static Vector3 Abs( this Vector3 v )
    {
        return new Vector3( Mathf.Abs( v.x ), Mathf.Abs( v.y ), Mathf.Abs( v.z ) );
    }

    public static int LargestAxis( this Vector3 v )
    {
        Vector3 a = v.Abs();

        return a.x > a.y ?
            ( a.x > a.z ? 0 : 2 ) :
            ( a.y > a.z ? 1 : 2 );
    }

    public static Vector3 _xz0( this Vector3 v )
    {
        return new Vector3( v.x, v.z, 0 );
    }

    public static Vector3 xz( this Vector3 v )
    {
        return new Vector3( v.x, 0, v.z );
    }

    public static Vector3 RotateCW( this Vector3 v )
    {
        return new Vector3( v.z, v.y, -v.x );
    }

    public static Vector3 RotateCCW( this Vector3 v )
    {
        return new Vector3( -v.z, v.y, v.x );
    }
    
    public static Vector3 ToAbsTurnVector( this Vector3 limitInVector3 )
    {
        while( limitInVector3.y > 180 )
        {
            limitInVector3.y -= 360;
        }
        while( limitInVector3.y < -180 )
        {
            limitInVector3.y += 360;
        }
        while( limitInVector3.x > 180 )
        {
            limitInVector3.x -= 360;
        }
        while( limitInVector3.x < -180 )
        {
            limitInVector3.x += 360;
        }
        while( limitInVector3.z > 180 )
        {
            limitInVector3.z -= 360;
        }
        while( limitInVector3.z < -180 )
        {
            limitInVector3.z += 360;
        }

        return limitInVector3;
    }

    public static Vector3 x0y( this Vector2 v2)
    {
        return new Vector3(v2.x, 0, v2.y );
    }
}

﻿using UnityEngine;
using System.Collections;
using System;

public static class SafeStart
{
    public static IEnumerator CheckCondition( Func<bool> condition, Action callback )
    {
        while( !condition() )
        {
            yield return new WaitForFixedUpdate();
        }

        if( callback != null )
        {
            callback();
        }
    }

    public static IEnumerator CheckNotNull( Func<object> value, Action callback )
    {        
        while( value() == null )
        {
            yield return new WaitForFixedUpdate();
        }

        if( callback != null )
        {
            callback();
        }
    }
}



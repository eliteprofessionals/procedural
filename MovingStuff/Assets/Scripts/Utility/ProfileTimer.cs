﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ProfileTimer
{
    protected string name;
    protected long start;

    public ProfileTimer( string name )
    {
        this.name = name;
        this.start = DateTime.Now.Ticks;
    }

    public void End()
    {
        long delta = DateTime.Now.Ticks - start;

        Debug.Log( name + ": " + (double)delta / 1000000.0 );
    }
}

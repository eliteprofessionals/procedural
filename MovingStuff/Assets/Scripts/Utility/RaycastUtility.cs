﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class RaycastUtility
{
    public static GameObject ObjectBetween( Vector3 from, Vector3 to, int mask = ~0, bool DebugShowRay = false )
    {
        Ray ray = new Ray( from, ( to - from ).normalized );

        float distance = ( to - from ).magnitude;

        RaycastHit info;
        if( Physics.Raycast( ray, out info, distance, mask ) )
        {
            if( DebugShowRay )
            {
                Debug.DrawRay( ray.origin, ray.direction );
            }
            return info.collider.gameObject;
        }

        return null;
    }
}

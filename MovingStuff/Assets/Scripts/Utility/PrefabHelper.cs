﻿
//#if UNITY_EDITOR
//using UnityEditor;
//#endif
//using System;
//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using Object = UnityEngine.Object;
//using System.Reflection;


//[ExecuteInEditMode]
//public class PrefabHelper : MonoBehaviour
//{
//    public PrefabReference reference;
//    private Vector3 position;
//    private bool forcePositionUpdate;

//    void Update()
//    {
//        if( forcePositionUpdate )
//        {
//            transform.localPosition = position;
//            forcePositionUpdate = false;
//        }
//        else
//        {
//            position = transform.localPosition;
//        }
//    }

//    void Start()
//    {
//        if( reference != null )
//        {
//            reference.OnPrefabChanged += EvaluateSceneUpdate;
//        }


//#if UNITY_EDITOR

//#else
//                Destroy(this);
//#endif
//    }

//#if UNITY_EDITOR

//    public void ReplaceNested( PrefabHelper ReplacementPrefab )
//    {
//        PrefabHelperReferences.UpdateReferences();
//        GameObject oldPrefab = ReplacementPrefab.GetComponent<PrefabHelper>().reference.ReferenceObject;

//        if( ReplacementPrefab.gameObject != ReplacementPrefab.reference.ReferenceObject )
//        {
//            Vector3 pos = ReplacementPrefab.transform.position;

//            ReplacementPrefab.transform.parent = null;

//            GameObject replacedPrefab = PrefabUtility.ReplacePrefab(ReplacementPrefab.gameObject, oldPrefab );
//            ReplacementPrefab.GetComponent<PrefabHelper>().reference.ReferenceObject = replacedPrefab;

//            replacedPrefab.transform.position = Vector3.zero;
//            ReplacementPrefab.transform.position = pos;
//        }

//        List<PrefabReference> affectedPrefabs = new List<PrefabReference>();

//        foreach( PrefabReference r in PrefabHelperReferences.AllReferences )
//        {
//            List<PrefabHelper> checkPrefabs = new List<PrefabHelper>();
//            checkPrefabs.AddRange( r.ReferenceObject.GetComponentsInChildren<PrefabHelper>() );

//            foreach( PrefabHelper helper in checkPrefabs )
//            {
//                if( helper.reference == ReplacementPrefab.GetComponent<PrefabHelper>().reference && isDirectChild( helper, r ) ) //If a prefab contains the changed prefab as a child.
//                {
//                    affectedPrefabs.Add( r );
//                }
//            }
//        }

//        foreach( PrefabReference prefab in affectedPrefabs )
//        {
//            GameObject newPrefab = PrefabUtility.InstantiatePrefab( prefab.ReferenceObject ) as GameObject;

//            List<PrefabHelper> requireUpdates = new List<PrefabHelper>();

//            requireUpdates.AddRange( newPrefab.GetComponentsInChildren<PrefabHelper>( true ).Where( value => value.reference == ReplacementPrefab.reference ) );
//            requireUpdates.ForEach( swapPrefab => ReplaceTransform( swapPrefab.transform, Instantiate( ReplacementPrefab.reference.ReferenceObject ) as GameObject ) );

//            GameObject replacedPrefab = PrefabUtility.ReplacePrefab(newPrefab, prefab.ReferenceObject );
//            newPrefab.GetComponent<PrefabHelper>().reference.ReferenceObject = replacedPrefab;

//            ReplaceNested( replacedPrefab.GetComponent<PrefabHelper>() );

//            DestroyImmediate( newPrefab );
//        }

//        reference.CallChange();
//        ResetPosition();
//    }

//    void EvaluateSceneUpdate()
//    {
//        if( this == null )
//        {
//            //Debug.Log( name + ", removed" );
//            return;
//        }
//        if( !HasPrefabAsParent )
//        {
//            Vector3 oldPos = transform.localPosition;
//            GameObject asPrefab = PrefabUtility.InstantiatePrefab( reference.ReferenceObject ) as GameObject;
//            ReplaceTransform( transform, asPrefab );
//            asPrefab.GetComponent<PrefabHelper>().position = oldPos;
//            asPrefab.GetComponent<PrefabHelper>().forcePositionUpdate = true;
//        }
//    }

//    public bool HasPrefabAsParent
//    {
//        get
//        {
//            if( transform.parent != null )
//            {
//                Transform parentObject = transform.parent;
//                while( parentObject != null )
//                {
//                    if( parentObject.GetComponent<PrefabHelper>() )
//                    {
//                        return true;
//                    }
//                    else
//                    {
//                        parentObject = parentObject.transform.parent;
//                    }
//                }
//            }
//            return false;
//        }
//    }

//    /// <summary>
//    /// Checks if a prefabhelper is directly below a prefab reference, allowing to check if there's one level of nested prefabs between the compared.
//    /// </summary>
//    /// <param name="child"></param>
//    /// <param name="parentReference"></param>
//    /// <returns></returns>
//    private bool isDirectChild( PrefabHelper child, PrefabReference parentReference )
//    {
//        if( parentReference != child.reference )
//        {
//            Transform ClosestPrefabParentToNested = child.transform.parent;
//            while( ClosestPrefabParentToNested != null )
//            {
//                if( ClosestPrefabParentToNested.GetComponent<PrefabHelper>() )
//                {
//                    if( ClosestPrefabParentToNested.GetComponent<PrefabHelper>().reference == parentReference )
//                    {
//                        return true;
//                    }
//                    return false;
//                }

//                ClosestPrefabParentToNested = ClosestPrefabParentToNested.transform.parent;
//            }
//        }
//        return false;
//    }


//    /// <summary>
//    /// Resets the position to what it was on the previous update
//    /// </summary>
//    void ResetPosition()
//    {
//        if( this != null )
//        {
//            transform.localPosition = position;
//        }
//    }

//    /// <summary>
//    /// Replaces a transform with another including its parent/position/rotation
//    /// </summary>
//    /// <param name="t"></param>
//    /// <param name="replacewith"></param>
//    public void ReplaceTransform( Transform t, GameObject replacewith )
//    {
//        ReplaceTransform( t, replacewith.transform );
//    }

//    /// <summary>
//    /// Replaces a transform with another including its parent/position/rotation
//    /// </summary>
//    /// <param name="t"></param>
//    /// <param name="replacewith"></param>
//    public void ReplaceTransform( Transform t, Transform replacewith )
//    {
//        TransformData data = getTransformData(t);
//        ApplyTransformData( replacewith, data );
//        replacewith.transform.parent = t.transform.parent;
//        replacewith.transform.position = t.transform.position;

//        t.name = "REMOVE!"; //track if objects have been destroyed..
//        DestroyImmediate( t.gameObject );
//    }

//    public TransformData getTransformData( Transform t )
//    {
//        Transform parent = t.transform.parent;
//        t.parent = null;
//        TransformData data = new TransformData(t.transform);
//        t.parent = parent;
//        return data;
//    }

//    public struct TransformData
//    {
//        public TransformData( Transform from )
//        {
//            position = from.position;
//            rotation = from.rotation;
//        }

//        public TransformData( Vector3 Pos, Quaternion Rotation )
//        {
//            position = Pos;
//            rotation = Rotation;
//        }

//        public Vector3 position;
//        public Quaternion rotation;
//    }

//    private void ApplyTransformData( Transform t, TransformData data )
//    {
//        if( this != null && transform != null )
//        {
//            transform.position = data.position;
//            transform.rotation = data.rotation;
//        }
//    }

//#endif
//}

//public static class PrefabHelperReferences
//{
//    public static List<GameObject> DestoryOnComplete = new List<GameObject>();
//    public static List<PrefabReference> AllReferences = new List<PrefabReference>();

//    public static void UpdateReferences() //Maybe load this only once
//    {
//        AllReferences.Clear();
//        AllReferences.AddRange( Resources.LoadAll<PrefabReference>( "" ) );
//    }

//    public static void DestroyObjects()
//    {
//        for( ; DestoryOnComplete.Count > 0; )
//        {
//            Object.DestroyImmediate( DestoryOnComplete[0] );
//            DestoryOnComplete.RemoveAt( 0 );
//        }
//    }
//}

﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class JPSPathfinder : AStarGridPathfinder
{
    public bool[] isDiagonal = { 
		true, false, true, false, false, true, false, true
	};
    
    public override List<Int3> DefaultGetNeighbors( Int3 parent, Int3 current )
    {
        List<Int3> result = new List<Int3>();

        if( current == CurrentSource ) 
        {
            for( int i = 0; i < 8; ++i )
            {
                if( isDiagonal[i] )
                    AddDiagonalJump( current, Int3.HorizontalNeighbors8[i], ref result );
                else 
                    AddJump( current, Int3.HorizontalNeighbors8[i], ref result );
            }
        }
        else 
        {
            Int3 normalizedDirection = ( current - parent ).normalized;

            int index = Int2.Neighbors8Index( normalizedDirection.xz );

            if( isDiagonal[index] )
            {
                bool jumpDiagonal = true;
                if( AddJump( current, normalizedDirection._x00, ref result ) )
                    jumpDiagonal = false;
                if( AddJump( current, normalizedDirection._00z, ref result ) )
                    jumpDiagonal = false;

                if( jumpDiagonal )
                    AddDiagonalJump( current, normalizedDirection, ref result );
            }
            else
            {
                if( IsJumpPoint( current, normalizedDirection ) )
                {
                    int tangentIndex = Mathf.Abs( normalizedDirection.x ) > Mathf.Abs( normalizedDirection.z ) ? 2 : 0;
                    Int3 tangent = new Int3( 0 );
                    tangent[tangentIndex] = 1;

                    AddJump( current, tangent, ref result );
                    AddJump( current, -tangent, ref result );
                    AddDiagonalJump( current, tangent + normalizedDirection, ref result );
                    AddDiagonalJump( current, -tangent + normalizedDirection, ref result );
                }

                AddJump( current, normalizedDirection, ref result );
            }

            if( GetUserNeighbors != null )
            {
                result.AddRange( GetUserNeighbors( parent, current ) );
            }
        }

        return result;
    }

    protected bool AddJump( Int3 origin, Int3 direction, ref List<Int3> result )
    {
        Int3 jump;
        if( Jump( origin, direction, out jump ) && jump != origin )
        {
            result.Add( jump );
            return true;
        }
        return false;
    }

    protected bool AddDiagonalJump( Int3 origin, Int3 direction, ref List<Int3> result )
    {
        Int3 jump;
        if( DiagonalJump( origin, direction, out jump ) )
        {
            result.Add( jump );
            return true;
        }
        return false;
    }

    protected bool DiagonalJump( Int3 origin, Int3 direction, out Int3 point )
    {
        bool hitJumpPoint = false;

        Int3 current = origin;
        Int3 previous;

        do
        {
            previous = current;
            current += direction;

            Int3 straight;
            if( Jump( current, direction._x00, out straight ) || Jump( current, direction._00z, out straight ) )
            {
                hitJumpPoint = true;
                break;
            }
        }
        while( GetIsNodeValid( previous, current ) );

        if( !hitJumpPoint )
        {
            current -= direction;
        }

        point = current;

        return hitJumpPoint;
    }

    protected bool Jump( Int3 origin, Int3 direction, out Int3 point )
    {
        if( !GetIsNodeValid( origin, origin ) )        
        {
            point = origin;
            return false;
        }

        Int3 current = origin;
        Int3 previous;

        bool hitJumpPoint = false;

        do
        {
            previous = current;
            current += direction;

            if( IsJumpPoint( current, direction ) )
            {
                hitJumpPoint = true;
                break;
            }
        }
        while( GetIsNodeValid( previous, current ) );

        if( !hitJumpPoint )
        {
            current -= direction;
        }

        point = current;

        return hitJumpPoint;
    }

    protected bool IsJumpPoint( Int3 position, Int3 direction )
    {
        if( position == CurrentTarget ) return true;

        int index = Int2.Neighbors8Index( direction.xz );

        if( isDiagonal[index] ) return false;
        
        int tangentAxis = Mathf.Abs( direction.x ) > Mathf.Abs( direction.z ) ? 2 : 0;
        Int3 tangent = new Int3( 0 );
        tangent[tangentAxis] = 1;

        Int3 parent = position - direction;

        if( !GetIsNodeValid( parent, parent + tangent ) && GetIsNodeValid( position, position + tangent ) )
            return true;

        if( !GetIsNodeValid( parent, parent - tangent ) && GetIsNodeValid( position, position - tangent ) )
            return true;

        return false;
    }
}

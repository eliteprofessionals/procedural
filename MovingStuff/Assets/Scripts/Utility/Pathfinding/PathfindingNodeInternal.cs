﻿


public class PathfindingNodeInternal<T, V>
{
	public PathfindingNodeInternal( T position, V identifier, float f, float g, PathfindingNodeInternal<T, V> cameFrom )
	{
		fScore = f;
		gScore = g;
		this.position = position;
        this.identifier = identifier;
		this.cameFrom = cameFrom;
	}

	public float fScore;
	public float gScore;
	public T position;
    public V identifier;
	public PathfindingNodeInternal<T, V> cameFrom;
}
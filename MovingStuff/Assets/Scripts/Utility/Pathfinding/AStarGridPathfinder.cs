﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AStarGridPathfinder
{
	public delegate List<Int3> GetNeighborsFnc( Int3 parent, Int3 current );
	public delegate List<Int3> GetUserNeighborsFnc( Int3 parent, Int3 current );
    public delegate bool GetIsNodeValidFnc( Int3 from, Int3 to );
	public delegate float GetFScoreFnc( Int3 from, Int3 to );
	public delegate float GetGScoreFnc( Int3 from, Int3 to );

	public GetIsNodeValidFnc GetIsNodeValid { get; set; }
	public GetNeighborsFnc GetNeighbors { get; set; }
	public GetFScoreFnc GetFScore { get; set; }
	public GetGScoreFnc GetGScore { get; set; }
	public GetUserNeighborsFnc GetUserNeighbors { get; set; }

	protected Int3 CurrentTarget = Int3.zero;
    protected Int3 CurrentSource = Int3.zero;

	public AStarGridPathfinder()
	{
		GetGScore = DefaultGScore;
		GetFScore = DefaultFScore;
		GetNeighbors = DefaultGetNeighbors;
	}

	public float DefaultFScore( Int3 from, Int3 to )
	{
		return Vector3.Distance( (Vector3)from, (Vector3)to );
	}

	public float DefaultGScore( Int3 from, Int3 to )
	{
		return Vector3.Distance( (Vector3)from, (Vector3)to );
	}

	protected bool CheckIsInitialized()
	{
		return GetIsNodeValid != null && GetNeighbors != null && GetFScore != null && GetGScore != null;
	}
    
	public virtual List<Int3> DefaultGetNeighbors( Int3 parent, Int3 current )
	{
		List<Int3> result = new List<Int3>();
		foreach( Int3 offset in Int3.HorizontalNeighbors8 )
		{
			Int3 neighbor = current + offset;
			if( GetIsNodeValid( current, neighbor ) )
			{ 
				result.Add( neighbor );
			}
		}

		if( GetUserNeighbors != null )
		{
			result.AddRange( GetUserNeighbors( parent, current ) );
		}

		return result;
	}
	
	public virtual List<Int3> Find( Int3 start, Int3 end, bool allowStraightLine = false, bool breakAtNeighbor = false )
	{
        if( allowStraightLine && start.y == end.y )
        {
            bool lineValid = true;
            Bresenham3D iterator = new Bresenham3D( (Vector3)start, (Vector3)end );
            Vector3 previous = (Vector3)start;
            foreach( Vector3 position in iterator )
            {
                if( !GetIsNodeValid( new Int3( previous ), new Int3( position ) ) )
                {
                    lineValid = false;
                    break;
                }
                previous = position;
            }

            if( lineValid )
            {
                return new List<Int3>( new Int3[]{ end } );
            }
        }

        CurrentSource = start;
		CurrentTarget = end;		

		HashSet<Int3> closed = new HashSet<Int3>();
		Dictionary<Int3, PathfindingNodeInternal<Int3, Int3>> open = new Dictionary<Int3, PathfindingNodeInternal<Int3, Int3>>();
		PriorityQueue<PathfindingNodeInternal<Int3, Int3>> openPQ = new PriorityQueue<PathfindingNodeInternal<Int3, Int3>>();

		float f = GetFScore( start, end );
		PathfindingNodeInternal<Int3, Int3> startNode = new PathfindingNodeInternal<Int3, Int3>( start, start, f, 0.0f, null );
		open.Add( start, startNode );
		openPQ.Add( f, startNode );
		
		while( open.Count > 0 )
		{
            if( open.Count > 1000 )
            {
                Debug.LogError( "Too many open nodes in pathfinder (>10000), possible bug in f/gscore or getneighbors" );
                break;
            }

			PathfindingNodeInternal<Int3, Int3> current = openPQ.RemoveMin();
			open.Remove( current.position );

			if( current.position == end || ( breakAtNeighbor && ( current.position - end ).Magnitude() < 1.5f ) )
			{
				List<Int3> path = new List<Int3>();
				path.Add( current.position );
				while( current.cameFrom != null )
				{
					current = current.cameFrom;
					path.Insert( 0, current.position );
				}
				return path;
			}

			closed.Add( current.position );

			Int3 from = current.cameFrom == null ? current.position : current.cameFrom.position;
			List<Int3> neighbors = GetNeighbors( from, current.position );
            
			foreach( Int3 neighbor in neighbors )
			{
				if( closed.Contains( neighbor ) || !GetIsNodeValid( current.position, neighbor ) )
					continue;

				float tg = current.gScore + GetGScore( current.position, neighbor );

				bool contains = open.ContainsKey( neighbor );
				if( !contains || tg < open[neighbor].gScore )
				{
					PathfindingNodeInternal<Int3, Int3> neighborNode = contains ? open[neighbor] : new PathfindingNodeInternal<Int3, Int3>( neighbor, neighbor, tg + GetFScore( neighbor, end ), tg, current );

					if( !contains )
					{
						open.Add( neighbor, neighborNode );
						openPQ.Add( neighborNode.fScore, neighborNode );
					}
				}
			}
		}

		return null;
	}
}

﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static T RandomElement<T>( this List<T> list )
    {
        if( list.Count == 0 )
        {
            throw new System.IndexOutOfRangeException();
        }
        
        return list[Random.Range( 0, list.Count )];
    }

    public static T RemoveRandom<T>( this List<T> list )
    {
        T result = RandomElement( list );

        list.Remove( result );

        return result;
    }

    public static void Shuffle<T>( this IList<T> list )
    {
        int n = list.Count;
        while( n > 1 )
        {
            n--;
            int k = (int)( Random.value * (float)n );
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
            
        }  
    }
}

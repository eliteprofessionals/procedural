﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum TileSide
{
	Up,
	Right,
	Down,
	Left
}

[System.Serializable]
public class TileBorderMap<T>
{
	[SerializeField] protected T[] bordersX;
	[SerializeField] protected T[] bordersY;


	[SerializeField] protected T[] _bordersX;
	[SerializeField] protected T[] _bordersY;
	[SerializeField] protected Int3 size;

	public Int3 Size { get { return size; } set { size = value; } }
	 
	public TileBorderMap( Int3 size )
	{
		size = Int3.Max( size, new Int3( 1 ) );
		this.size = size;

		_bordersX = new T[ ( size + new Int3( 1, 0, 0 ) ).Area ];
		_bordersY = new T[ ( size + new Int3( 0, 0, 1 ) ).Area ];
	}

	public void MoveTMP()
	{ 
		if( bordersX != null )
		{
			_bordersX = new T[ ( size + new Int3( 1, 0, 0 ) ).Area ];
			_bordersY = new T[ ( size + new Int3( 0, 0, 1 ) ).Area ];
			
			for( int x = 0; x <= size.x; ++x )
			{
 				for( int y = 0; y <= size.z; ++y )
				{
					if( y < size.z )
					{ 
 						_bordersX[HorizontalIndexOf( new Int3( x, 0, y ) )] = bordersX[HorizontalIndexOf( new Int3( x, 0, y ) )];
					}
					
					if( x < size.x )
					{ 
						_bordersY[VerticalIndexOf( new Int3( x, 0, y ) )] = bordersY[VerticalIndexOf( new Int3( x, 0, y ) )];
					}
				}
			}
		}
	}
	
	public T GetBorder( Int3 position, TileSide tileSide )
	{
		switch( tileSide )
		{
		case TileSide.Down: return GetVerticalBorder( position );
		case TileSide.Up: return GetVerticalBorder( position + new Int3( 0, 0, 1 ) );
		case TileSide.Left: return GetHorizontalTile( position );
		case TileSide.Right: return GetHorizontalTile( position + new Int3( 1, 0, 0 ) );
		default: return default( T );
		}
	}

	protected bool IsInsideHorizontal( Int3 position )
	{
		return position.x >= 0 && position.y >= 0 && position.z >= 0 && position.x <= size.x && position.y < size.y && position.z < size.z;
	}

	protected bool IsInsideVertical( Int3 position )
	{
		return position.x >= 0 && position.y >= 0 && position.z >= 0 && position.x < size.x && position.y < size.y && position.z <= size.z;
	}

	protected int HorizontalIndexOf( Int3 position )
	{
		return position.y * size.z * ( size.x + 1 ) + position.x * size.z + position.z;
	}

	protected int VerticalIndexOf( Int3 position )
	{
		return position.y * ( size.z + 1 ) * size.x + position.x + position.z * size.x;
	}
	
	protected T GetHorizontalTile( Int3 position )
	{
		if( IsInsideHorizontal( position ) )
		{ 
			return _bordersX[HorizontalIndexOf( position )];
		}

		return default( T );
	}

	protected T GetVerticalBorder( Int3 position )
	{
		if( IsInsideVertical( position ) )
		{ 
			return _bordersY[VerticalIndexOf( position )];
		}

		return default( T );
	}

	public void SetBorder( Int3 position, TileSide tileSide, T value )
	{
		switch( tileSide )
		{
		case TileSide.Down: SetVerticalBorder( position, value ); break;
		case TileSide.Up: SetVerticalBorder( position + new Int3( 0, 0, 1 ), value ); break;
		case TileSide.Left: SetHorizontalTile( position, value ); break;
		case TileSide.Right: SetHorizontalTile( position + new Int3( 1, 0, 0 ), value ); break;
		} 
	}
	
	
	protected void SetHorizontalTile( Int3 position, T value )
	{
		if( IsInsideHorizontal( position ) ) 
			_bordersX[HorizontalIndexOf( position )] = value;
	}

	protected void SetVerticalBorder( Int3 position, T value )
	{
		if( IsInsideVertical( position ) )
			_bordersY[VerticalIndexOf( position )] = value;
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// 'Typedefs' for generic classes.
// This should be used to help unity serialize;
// unity does not serialize generic classes, unless they're the parent of a non generic class.

[System.Serializable]
public class OccupationMap3D : LinearMap3D<bool>
{
    public OccupationMap3D() : base() { }
    public OccupationMap3D( Int3 size ) : base( size ) { }
}
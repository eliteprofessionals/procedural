﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class LinearMap3D<T>
{
	public int Width { get { return width; } }
	public int Height { get { return height; } }
	public int Depth { get { return depth; } }
	public Int3 Size { get { return new Int3( width, height, depth ); } }

	[SerializeField]
	private int width;

	[SerializeField]
    private int height;

	[SerializeField]
	private int depth;

	[SerializeField]
	protected T[] data;

	public T this[Int3 index]
	{
		get { return GetValue( index ); }
		set { SetValue( index, value ); }
	}

	public T this[int x, int y, int z]
	{
		get { return GetValue( x, y, z ); }
		set { SetValue( x, y, z, value ); }
	}

	protected LinearMap3D() { }

	public LinearMap3D( Int3 dimensions )
	{
		width = dimensions.x;
		height = dimensions.y;
		depth = dimensions.z;

		data = new T[Width * Height * Depth];
	}

	public void InitializeWith( T value )
	{
		int size = Width * Height * Depth;

		for( int i = 0; i < size; ++i )
		{
			data[i] = value;
		}
	}

	public void GetNeighbors( Int3 current, ref List<Int3> neighbors, int radius = 1 )
	{
		int xMin = Mathf.Max( current.x - radius, 0 );
		int yMin = Mathf.Max( current.y - radius, 0 );
		int zMin = Mathf.Max( current.z - radius, 0 );

		int xMax = Mathf.Min( current.x + radius, Width - 1 );
		int yMax = Mathf.Min( current.y + radius, Height - 1 );
		int zMax = Mathf.Min( current.z + radius, Depth - 1 );

		for( int x = xMin; x <= xMax; ++x )
		{
			for( int y = yMin; y <= yMax; ++y )
			{
				for( int z = zMin; z <= zMax; ++z )
				{
					if( x == current.x && y == current.y && z == current.z )
						continue;

					neighbors.Add( new Int3( x, y, z ) );
				}
			}
		}
	}

	public void GetNeighborsStraight( Int3 current, ref List<Int3> neighbors, int radius = 1 )
	{
		int xMin = Mathf.Max( current.x - radius, 0 );
		int yMin = Mathf.Max( current.y - radius, 0 );
		int zMin = Mathf.Max( current.z - radius, 0 );

		int xMax = Mathf.Min( current.x + radius, Width - 1 );
		int yMax = Mathf.Min( current.y + radius, Height - 1 );
		int zMax = Mathf.Min( current.z + radius, Depth - 1 );

		for( int x = xMin; x <= xMax; ++x )
		{
			if( x == current.x )
				continue;

			neighbors.Add( new Int3( x, current.y, current.z ) );
		}

		for( int y = yMin; y <= yMax; ++y )
		{
			if( y == current.y )
				continue;

			neighbors.Add( new Int3( current.x, y, current.z ) );
		}

		for( int z = zMin; z <= zMax; ++z )
		{
			if( z == current.z )
				continue;

			neighbors.Add( new Int3( current.x, current.y, z ) );
		}
	}

	public void GetNeighborsXZ( Int3 current, ref List<Int3> neighbors )
	{
		int xMin = Mathf.Max( current.x - 1, 0 );
		int zMin = Mathf.Max( current.z - 1, 0 );

		int xMax = Mathf.Min( current.x + 1, Width - 1 );
		int zMax = Mathf.Min( current.z + 1, Depth - 1 );

		for( int x = xMin; x <= xMax; ++x )
		{
			for( int z = zMin; z <= zMax; ++z )
			{
				if( x == current.x && z == current.z )
					continue;

				neighbors.Add( new Int3( x, current.y, z ) );
			}
		}
	}

	protected bool InitializeMinMax( ref Int3 start, ref Int3 end )
	{
		if( start.x > end.x )
			MathUtil.Swap( ref start.x, ref end.x );
		if( start.y > end.y )
			MathUtil.Swap( ref start.y, ref end.y );
		if( start.z > end.z )
			MathUtil.Swap( ref start.z, ref end.z );

		Int3 oldStart = start;
		Int3 oldEnd = end;

		start.x = Mathf.Max( start.x, 0 );
		start.y = Mathf.Max( start.y, 0 );
		start.z = Mathf.Max( start.z, 0 );

		end.x = Mathf.Min( end.x, Width - 1 );
		end.y = Mathf.Min( end.y, Height - 1 );
		end.z = Mathf.Min( end.z, Depth - 1 );

		return oldStart != start || oldEnd != end; // check if we got clamped
	}



	public void SetValues( Int3 start, Int3 end, T value )
	{
		Int3 min = new Int3( start );
		Int3 max = new Int3( end );

		InitializeMinMax( ref min, ref max );

		for( int x = min[0]; x <= max[0]; ++x )
		{
			for( int y = min[1]; y <= max[1]; ++y )
			{
				for( int z = min[2]; z <= max[2]; ++z )
				{
					SetValueUnsafe( new Int3( x, y, z ), value );
				}
			}
		}
	}

	public bool IsInside( Int3 position )
	{
		return position.x >= 0 && position.y >= 0 && position.z >= 0 &&
			position.x < Width && position.y < Height && position.z < Depth;
	}

	public int IndexOf( Int3 position )
	{
		return position.x + position.y * Width + position.z * Width * Height;
	}

	public int IndexOf( int x, int y, int z )
	{
		return x + y * Width + z * Width * Height;
	}

	public void SetValue( int x, int y, int z, T value )
	{
		SetValue( new Int3( x, y, z ), value );
	}

	public void SetValue( Int3 position, T value )
	{
		if( IsInside( position ) )
		{
			data[IndexOf( position )] = value;
		}
	}

	public void SetValueUnsafe( Int3 position, T value )
	{
		data[IndexOf( position )] = value;
	}

	public void SetValueUnsafe( int index, T value )
	{
		data[index] = value;
	}

	public T GetValue( int x, int y, int z )
	{
		return GetValue( new Int3( x, y, z ) );
	}

	public T GetValue( Int3 position )
	{
		if( IsInside( position ) )
		{
			return data[IndexOf( position )];
		}
		return default( T );
	}

	public T GetValueUnsafe( Int3 position )
	{
		return data[IndexOf( position )];
	}

	public T GetValueUnsafe( int index )
	{
		return data[index];
	}

	public T[, ,] ToArray()
	{
		T[, ,] result = new T[width, height, depth];

		for( int x = 0; x < width; ++x )
		{
			for( int y = 0; y < height; ++y )
			{
				for( int z = 0; z < depth; ++z )
				{
					result[x, y, z] = data[IndexOf( x, y, z )];
				}
			}
		}

		return result;
	}

    public void Resize( Int3 newSize )
    {
        Int3 max = Int3.Min( Size, newSize );

        T[] newData = new T[newSize.x * newSize.y * newSize.z];

        BoxFillIterator iterator = new BoxFillIterator( Int3.zero, max );
        foreach( Int3 position in iterator )
        {
            newData[position.x + position.y * newSize.x + position.z * newSize.x * newSize.y] = GetValueUnsafe( position );
        }

        data = newData;
        width = newSize.x;
        height = newSize.y;
        depth = newSize.z;
    }
}
﻿using UnityEngine;
using System.Collections;

public class SpawnOnDestroy : MonoBehaviour
{
	public GameObject[] Spawn;
	
	public bool applicationQuitting = false;

	void OnApplicationQuit()
	{
		applicationQuitting = true;
	}

	void OnDestroy()
	{
		if( applicationQuitting ) return;

		foreach( GameObject s in Spawn )
		{
			Instantiate( s, transform.position, Quaternion.identity );
		}
	}
}

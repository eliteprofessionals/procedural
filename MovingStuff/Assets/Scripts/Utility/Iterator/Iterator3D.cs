﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineIterator3D : IEnumerable
{
    Bresenham3D line = null;

    public LineIterator3D( Int3 from, Int3 to )
    { 
        line = new Bresenham3D( (Vector3)from, (Vector3)to );
    }

    public IEnumerator GetEnumerator()
    {
        IEnumerator enumerator = line.GetEnumerator();
        
        while( enumerator.MoveNext() )
        {
            yield return new Int3( (Vector3)enumerator.Current );
        }

        yield break;
    }
}

public class BoxOutlineIterator : IEnumerable
{
    protected Int3 min;
    protected Int3 max;

    public BoxOutlineIterator( Int3 min, Int3 max )
    {
        this.min = min;
        this.max = max;
    }

    public IEnumerator GetEnumerator()
    {
        for( int x = min.x; x <= max.x; ++x )
        {
            yield return new Int3( x, min.y, min.z );
            yield return new Int3( x, max.y, min.z );
            yield return new Int3( x, min.y, max.z );
            yield return new Int3( x, max.y, max.z );
        }

        for( int y = min.y + 1; y < max.y; ++y )
        {
            yield return new Int3( min.x, y, min.z );
            yield return new Int3( max.x, y, min.z );
            yield return new Int3( min.x, y, max.z );
            yield return new Int3( max.x, y, max.z );
        }

		for( int z = min.z + 1; z < max.z; ++z )
		{
            yield return new Int3( min.x, min.y, z );
            yield return new Int3( max.x, min.y, z );
            yield return new Int3( min.x, max.y, z );
            yield return new Int3( max.x, max.y, z );
		}

        yield break;
    }
}

public class BoxFillIterator : IEnumerable
{
    protected Int3 min;
    protected Int3 max;

    public BoxFillIterator( Int3 _min, Int3 _max )
    {
        min = _min;
        max = _max;
        Int3.MakeMinMax( ref min, ref max );
    }

    public BoxFillIterator( BoxI box )
    {
        min = box.min;
        max = box.max;
    }

    public IEnumerator GetEnumerator()
    {
		for( int z = min.z; z < max.z; ++z )
		{
			for( int y = min.y; y < max.y; ++y )        
			{        
				for( int x = min.x; x < max.x; ++x )
				{
					yield return new Int3( x, y, z );
				}
			}
		}

        yield break;
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineIterator : IEnumerable
{
    Bresenham3D line = null;

    public LineIterator( Int2 from, Int2 to )
    { 
        line = new Bresenham3D( new Vector3( from.x, from.y, 0.0f ), new Vector3( to.x, to.y, 0.0f ) );
    }

    public IEnumerator GetEnumerator()
    {
        IEnumerator enumerator = line.GetEnumerator();
        
        while( enumerator.MoveNext() )
        {
            Vector3 p = (Vector3)enumerator.Current;

            yield return new Int2( (int)p.x, (int)p.y );
        }

        yield break;
    }
}

public class RectangleOutlineIterator : IEnumerable
{
    protected Int2 min;
    protected Int2 max;

    public RectangleOutlineIterator( Int2 min, Int2 max )
    {
        this.min = min;
        this.max = max;
    }

    public IEnumerator GetEnumerator()
    {
        for( int x = min.x; x <= max.x; ++x )
        {
            yield return new Int2( x, min.y );
            yield return new Int2( x, max.y );
        }

        for( int y = min.y + 1; y < max.y; ++y )
        {
            yield return new Int2( min.x, y );
            yield return new Int2( max.x, y );
        }

        yield break;
    }
}

public class RectangleFillIterator : IEnumerable
{
    protected Int2 min;
    protected Int2 max;

    public RectangleFillIterator( Int2 min, Int2 max )
    {
        this.min = min;
        this.max = max;
    }

    public IEnumerator GetEnumerator()
    {
        for( int y = min.y; y < max.y; ++y )        
        {        
            for( int x = min.x; x < max.x; ++x )
            {
                yield return new Int2( x, y );
            }
        }

        yield break;
    }
}

public class CircleOutlineIterator : IEnumerable
{
    protected int radius;
    protected Int2 center;

	public CircleOutlineIterator( Int2 center, int radius )
	{
        this.center = center;
        this.radius = radius;
    }


    public IEnumerator GetEnumerator()
    {		
		for( float ly = radius; ly >= -radius; --ly )
		{
			int wy = (int)ly + center.y;

			float f = ly / (float)( radius );

			float a = Mathf.Acos( f );

			int xLim = (int)Mathf.RoundToInt( Mathf.Sin( a ) * (float)radius );

			if( xLim == 1 )
			{
				yield return new Int2( center.x, wy );
			}
			else if( xLim > 1 )
			{
				yield return new Int2( center.x + ( -xLim + 1 ), wy );
				yield return new Int2( center.x + (  xLim - 1 ), wy );
			}

			for( int lx = -xLim + 1; lx <= xLim - 1; ++lx )
			{
				int wx = center.x + lx;

				yield return new Int2( wx, wy );
			}
		}

        yield break;
	}
}

public class CircleFillIterator : IEnumerable
{
    protected Int2 center;
    protected int radius;

	public CircleFillIterator( Int2 center, int radius )
	{
        this.center = center;
        this.radius = radius;

    }


    public IEnumerator GetEnumerator()
    {		
		for( float ly = radius; ly >= -radius; --ly )
		{
			int wy = (int)ly + center.y;

			float f = ly / (float)( radius );

			float a = Mathf.Acos( f );

			int xLim = (int)Mathf.RoundToInt( Mathf.Sin( a ) * (float)radius );

			for( int lx = -xLim + 1; lx <= xLim - 1; ++lx )
			{
				int wx = center.x + lx;

				yield return new Int2( wx, wy );
			}
		}

        yield break;
	}
}
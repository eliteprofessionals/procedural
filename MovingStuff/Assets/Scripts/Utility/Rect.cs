﻿using UnityEngine;
using System.Collections;

public static class RectExtention
{
    /// <summary>
    /// confines point to the bounds.
    /// </summary>
    /// <param name="rect"></param>
    /// <param name="contain"></param>
    /// <returns>contained point</returns>
    public static Vector2 Contain( this Rect rect, Vector2 contain )
    {
        Vector2 value = contain;
        if ( value.x > rect.xMax ) { value.x = rect.xMax; }
        if ( value.y > rect.yMax ) { value.y = rect.yMax; }
        if ( value.x < rect.xMin ) { value.x = rect.xMin; }
        if ( value.y < rect.yMin ) { value.y = rect.yMin; }
        return value;
    }
}

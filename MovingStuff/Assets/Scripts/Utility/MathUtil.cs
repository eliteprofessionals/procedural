﻿using UnityEngine;

public static class MathUtil
{
    public static float NormRand( int N )
    {
        float acc = 0.0f;
        for( int i = 0; i < N; ++i )
        {
            acc += Random.value;
        }
        return acc / (float)N;
    }

    public static void Swap<T>( ref T a, ref T b )
    {
        T tmp = b;
        b = a;
        a = tmp;
    }    

    public static int Mod( int x, int m )
    {
        return ( x % m + m ) % m;
    }

    public static float Mod( float x, float m )
    {
        return ( x % m + m ) % m;
    }

    public static int Fibonacci( int N )
    {
        int previous = 1;
        int result = 1;
        for( int i = 0; i < N; i++ )
        {
            previous = result;
            result += previous;
        }
        return result;
    }

    public static float Pack( float p )
    {
        return (float)( ( p + 1.0f ) / 2.0f );
    }

    //first-order intercept using absolute target position
    public static Vector3 FirstOrderIntercept( Vector3 shooterPosition, Vector3 shooterVelocity, float shotSpeed, Vector3 targetPosition, Vector3 targetVelocity )
    {
        Vector3 targetRelativePosition = targetPosition - shooterPosition;
        Vector3 targetRelativeVelocity = targetVelocity - shooterVelocity;
        float t = FirstOrderInterceptTime( shotSpeed, targetRelativePosition, targetRelativeVelocity );
        return targetPosition + t * ( targetRelativeVelocity );
    }

    //first-order intercept using relative target position
    public static float FirstOrderInterceptTime( float shotSpeed, Vector3 targetRelativePosition, Vector3 targetRelativeVelocity )
    {
        float velocitySquared = targetRelativeVelocity.sqrMagnitude;
        if( velocitySquared < 0.001f )
            return 0f;

        float a = velocitySquared - shotSpeed * shotSpeed;

        //handle similar velocities
        if( Mathf.Abs( a ) < 0.001f )
        {
            float t = -targetRelativePosition.sqrMagnitude /
            (
                2f * Vector3.Dot
                (
                    targetRelativeVelocity,
                    targetRelativePosition
                )
            );
            return Mathf.Max( t, 0f ); //don't shoot back in time
        }

        float b = 2f * Vector3.Dot( targetRelativeVelocity, targetRelativePosition );
        float c = targetRelativePosition.sqrMagnitude;
        float determinant = b * b - 4f * a * c;

        if( determinant > 0f )
        { //determinant > 0; two intercept paths (most common)
            float t1 = ( -b + Mathf.Sqrt( determinant ) ) / ( 2f * a ),
                    t2 = ( -b - Mathf.Sqrt( determinant ) ) / ( 2f * a );
            if( t1 > 0f )
            {
                if( t2 > 0f )
                    return Mathf.Min( t1, t2 ); //both are positive
                else
                    return t1; //only t1 is positive
            }
            else
                return Mathf.Max( t2, 0f ); //don't shoot back in time
        }
        else if( determinant < 0f ) //determinant < 0; no intercept path
            return 0f;
        else //determinant = 0; one intercept path, pretty much never happens
            return Mathf.Max( -b / ( 2f * a ), 0f ); //don't shoot back in time
    }

    public static Bounds ObjectBounds( GameObject gameObject )
    {
        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();

        float max = float.NegativeInfinity;
        float min = float.PositiveInfinity;

        Bounds bounds = new Bounds( new Vector3( min, min, min ), new Vector3( max, max, max ) );

        foreach( Renderer renderer in renderers )
        {
            bounds.Encapsulate( renderer.bounds );
        }

        return bounds;
    }

    public static Vector3 RandomVector3()
    {
        return new Vector3( Random.value, Random.value, Random.value );
    }
}
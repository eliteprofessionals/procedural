﻿//using UnityEngine;

//[System.Serializable]
//public struct DoorHole
//{
//    public Int3 Position;
//    public AttachableSide Side;

//    public GameObject doorObject;

//    public bool hasDoor {get { return doorObject != null; } }

//    public Int3 Neighbor { get { return Position + Int3.Neighbors6[(int)Side]; } }

//    public Int3 Direction { get { return Int3.Neighbors6[(int)Side]; } }

//    public DoorHole( Int3 position, AttachableSide side )
//    {
//        doorObject = null;
//        this.Position = position;
//        this.Side = side;
//    }

//    public void SetDoor(GameObject DoorObject)
//    {
//        doorObject = DoorObject;
//    }

//    public override bool Equals( System.Object obj )
//    {
//        return obj is DoorHole && this == (DoorHole)obj;
//    }

//    public override int GetHashCode()
//    {
//        return Position.GetHashCode() ^ Side.GetHashCode();
//    }

//    public static bool operator ==( DoorHole x, DoorHole y )
//    {
//        return x.Position == y.Position && x.Side == y.Side;
//    }

//    public static bool operator !=( DoorHole x, DoorHole y )
//    {
//        return !( x == y );
//    }

//}

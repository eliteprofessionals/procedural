﻿using UnityEngine;

public static class DebugUtil
{
	public static void DrawBounds( Bounds bounds, Color color, float duration, bool depthTest )
	{
		Debug.DrawLine( new Vector3( bounds.min.x, bounds.min.y, bounds.min.z ), new Vector3( bounds.max.x, bounds.min.y, bounds.min.z ), color, duration, depthTest );
		Debug.DrawLine( new Vector3( bounds.min.x, bounds.max.y, bounds.min.z ), new Vector3( bounds.max.x, bounds.max.y, bounds.min.z ), color, duration, depthTest );

		Debug.DrawLine( new Vector3( bounds.min.x, bounds.min.y, bounds.max.z ), new Vector3( bounds.max.x, bounds.min.y, bounds.max.z ), color, duration, depthTest );
		Debug.DrawLine( new Vector3( bounds.min.x, bounds.max.y, bounds.max.z ), new Vector3( bounds.max.x, bounds.max.y, bounds.max.z ), color, duration, depthTest );
		
		Debug.DrawLine( new Vector3( bounds.min.x, bounds.min.y, bounds.min.z ), new Vector3( bounds.min.x, bounds.min.y, bounds.max.z ), color, duration, depthTest );
		Debug.DrawLine( new Vector3( bounds.max.x, bounds.min.y, bounds.min.z ), new Vector3( bounds.max.x, bounds.min.y, bounds.max.z ), color, duration, depthTest );

		Debug.DrawLine( new Vector3( bounds.min.x, bounds.max.y, bounds.min.z ), new Vector3( bounds.min.x, bounds.max.y, bounds.max.z ), color, duration, depthTest );
		Debug.DrawLine( new Vector3( bounds.max.x, bounds.max.y, bounds.min.z ), new Vector3( bounds.max.x, bounds.max.y, bounds.max.z ), color, duration, depthTest );

		Debug.DrawLine( new Vector3( bounds.min.x, bounds.min.y, bounds.min.z ), new Vector3( bounds.min.x, bounds.max.y, bounds.min.z ), color, duration, depthTest );
		Debug.DrawLine( new Vector3( bounds.max.x, bounds.min.y, bounds.min.z ), new Vector3( bounds.max.x, bounds.max.y, bounds.min.z ), color, duration, depthTest );
		Debug.DrawLine( new Vector3( bounds.max.x, bounds.min.y, bounds.max.z ), new Vector3( bounds.max.x, bounds.max.y, bounds.max.z ), color, duration, depthTest );
		Debug.DrawLine( new Vector3( bounds.min.x, bounds.min.y, bounds.max.z ), new Vector3( bounds.min.x, bounds.max.y, bounds.max.z ), color, duration, depthTest );
	}
}
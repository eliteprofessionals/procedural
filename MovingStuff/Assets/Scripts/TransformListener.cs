﻿using UnityEngine;
using System;
using System.Collections;

public class TransformListener : MonoBehaviour
{
    public event Action<Vector3> hasMoved = delegate { };

    private Vector3 lastPosition;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (lastPosition != transform.position)
        {
            hasMoved(transform.position);
            lastPosition = transform.position;
        }
    }
}

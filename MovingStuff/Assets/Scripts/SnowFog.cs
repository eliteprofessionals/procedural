﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnowFog : MonoBehaviour
{

    //private static readonly int OverlaySize = 128;

    protected Plane snowPlane;
    public GameObject snowCamera;

    public float planeSize;

    void Start()
    {
        snowPlane = new Plane( Vector3.up, -transform.position.y );

        GenerateOverlay( LocalPlayer.Instance.ActiveBlock, MovingMesh.Instance.HighLodVertexMultiplier, MovingMesh.Instance.planeSizeX / 2 );
    }

    private void GenerateOverlay( Vector2 Position, int OverlaySize, int OrthoSize )
    {
        float size = OrthoSize;

        planeSize = OverlaySize;

        snowCamera.transform.position = new Vector3( 0, snowCamera.transform.position.y, 0 );
        snowCamera.GetComponent<Camera>().orthographicSize = size;


        transform.localScale = new Vector3( 1, 1, 1 );


        // Disabled because it interferes with itself and the weather FX.
#if true
        Mesh mesh = new Mesh();
        List<Vector3> vertices = new List<Vector3>();
        List<int> indices = new List<int>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();

        RectangleFillIterator iterator = new RectangleFillIterator( Int2.zero, new Int2( OverlaySize ) );
        foreach( Int2 position in iterator )
        {
            int N = vertices.Count;
            vertices.Add( new Vector3( position.x, 0, position.y ) );

            uvs.Add( new Vector2( position.x, position.y ) / (float)OverlaySize );

            if( position.x < OverlaySize - 1 && position.y < OverlaySize - 1 )
            {
                indices.Add( N + 1 );
                indices.Add( N );
                indices.Add( N + OverlaySize );

                indices.Add( N + 1 + OverlaySize );
                indices.Add( N + 1 );
                indices.Add( N + OverlaySize );
            }
        }

        mesh.SetVertices( vertices );
        mesh.SetUVs( 0, uvs );
        mesh.SetIndices( indices.ToArray(), MeshTopology.Triangles, 0 );

        GetComponent<MeshFilter>().mesh = mesh;

        gameObject.transform.rotation = Quaternion.identity;
        gameObject.transform.position = new Vector3( 0.0f, 0.5f, 0.0f );
#endif
    }
}

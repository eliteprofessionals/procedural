﻿using UnityEngine;
using System.Collections;

public class CreateTexture : MonoBehaviour
{

    public int sizeX;
    public int sizeY;

    public Transform followTransform;

    public Texture2D snowCovered;
    public Texture2D ObjectSnowDampeningMask;
    public Material passTo;

    public int SnowMaskBlur;

    public int multiplier = 64;

    public int worldSize;
    public int Scale; //TODO: MAKE THE SNOW MAP SCALE WITH WROLDLLS

    void Start()
    {
        if( snowCovered == null )
        {
            snowCovered = new Texture2D( sizeX * multiplier, sizeY * multiplier, TextureFormat.Alpha8, true );

            for( int x = 0; x < sizeX * multiplier; x++ )
            {
                for( int y = 0; y < sizeY * multiplier; y++ )
                {
                    snowCovered.SetPixel( x, y, new Color( 0, 0, 0, 0 ) );
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Int2 pos = TranslateToVertex( followTransform.transform.position );
        int AverageOver = 256 / 8;

        int size = ObjectSnowDampeningMask.width / AverageOver;

        for( int y = 0; y < size; y++ )
        {
            for( int x = 0; x < size; x++ )
            {
                Int2 pixelPos = new Int2( pos.x + ( x - ( size / 2 ) ), pos.y + ( y - ( size / 2 ) ) );
                Color color = Average( new Int2( AverageOver, AverageOver ), new Int2( x * AverageOver, y * AverageOver ), ObjectSnowDampeningMask );

                if( color.a > snowCovered.GetPixel( pixelPos.x, pixelPos.y ).a )
                {
                    snowCovered.SetPixel( pixelPos.x, pixelPos.y, color );
                }
            }
        }

        //snowCovered.SetPixel( pos.x, pos.y, new Color( 0, 0, 0, 1 ) );

        snowCovered.Apply();
        passTo.SetTexture( "_SnowCrumpled", snowCovered );
    }

    private Color Average( Int2 readSize, Int2 Start, Texture2D read )
    {
        Color average = new Color( 0, 0, 0, 0 );

        for( int y = 0; y < readSize.y; y++ )
        {
            for( int x = 0; x < readSize.x; x++ )
            {
                average += read.GetPixel( Start.x + x, Start.y + y );
            }
        }

        average /= ( ( readSize.x * readSize.y ) * 4 );
        return average;
    }


    public Int2 TranslateToVertex( Vector3 position )
    {
        int offset = ( sizeX * multiplier ) / 2;
        Int2 pos = new Int2( Mathf.RoundToInt( position.x + offset ), Mathf.RoundToInt( position.z + offset ) );
        return pos;
    }
}

﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;


public class SimpleMove : MonoBehaviour
{
    public float Speed;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = CrossPlatformInputManager.GetAxisRaw( "Horizontal" );
        float vertical = CrossPlatformInputManager.GetAxisRaw( "Vertical" );

        if( horizontal != 0f )
        {
            transform.position += transform.TransformDirection( new Vector3( horizontal, 0, 0 ).normalized * Speed * Time.deltaTime );
        }
        if( vertical != 0f )
        {
            transform.position += transform.TransformDirection( new Vector3( 0, 0, vertical ).normalized * Speed * Time.deltaTime );
        }
    }
}
